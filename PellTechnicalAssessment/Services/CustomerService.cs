﻿using Microsoft.EntityFrameworkCore;
using PellTechnicalAssessment.Data;
using PellTechnicalAssessment.Domain;

namespace PellTechnicalAssessment.Services;

public class CustomerService
{
    private readonly PellSoftwareTechnicalAssessmentDbContext _dbContext;

    public CustomerService(
        PellSoftwareTechnicalAssessmentDbContext dbContext
        )
    {
        this._dbContext = dbContext;
    }

    /// <summary>
    /// Returns all customers from the database
    /// </summary>
    /// <returns></returns>
    public async Task<List<Customer>> GetAllCustomers()
    {
        return await this._dbContext.Customers
            .AsNoTracking()
            .Take(2)
            .ToListAsync();
    }

}
