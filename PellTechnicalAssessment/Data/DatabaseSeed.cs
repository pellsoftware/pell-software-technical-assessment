﻿using PellTechnicalAssessment.Domain;

namespace PellTechnicalAssessment.Data;

public static class DatabaseSeed
{
    public static void Seed(PellSoftwareTechnicalAssessmentDbContext context)
    {
        if (context.Customers.Any())
        {
            return;
        }

        context.Customers.AddRange([
            new Customer{
                EmailAddress = "careers@pellsoftware.com",
                Name = "Pell"
            }
            ]);

        context.SaveChanges();
    }
}
