﻿using Microsoft.EntityFrameworkCore;
using PellTechnicalAssessment.Domain;

namespace PellTechnicalAssessment.Data;

public class PellSoftwareTechnicalAssessmentDbContext : DbContext
{
    public DbSet<Customer> Customers { get; set; }

    public PellSoftwareTechnicalAssessmentDbContext(
        DbContextOptions<PellSoftwareTechnicalAssessmentDbContext> options
        ) : base(options)
    {
    }
}
