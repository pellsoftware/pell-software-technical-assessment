﻿using Microsoft.EntityFrameworkCore;
using PellTechnicalAssessment.Data;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddControllersWithViews();

builder.Services.AddDbContext<PellSoftwareTechnicalAssessmentDbContext>(options => options.UseSqlServer(builder.Configuration.GetConnectionString("Default")));

var app = builder.Build();

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Error/Index");
    app.UseHsts();
}

app.UseHttpsRedirection();
app.UseStaticFiles();

app.UseRouting();

app.UseAuthorization();

app.MapControllerRoute(
    name: "default",
    pattern: "{controller=Customer}/{action=Index}/{id?}");

// database migrations and seed. make sure connection string is set correctly.
var scope = app.Services.CreateScope();
var dbContext = scope.ServiceProvider.GetRequiredService<PellSoftwareTechnicalAssessmentDbContext>();

// any Entity Framework migrations created using the `add-migrations` tool will be automatically applied here.
dbContext.Database.Migrate();
DatabaseSeed.Seed(dbContext);

app.Run();
