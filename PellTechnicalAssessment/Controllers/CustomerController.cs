﻿using Microsoft.AspNetCore.Mvc;
using PellTechnicalAssessment.Services;

namespace PellTechnicalAssessment.Controllers;

public class CustomerController : Controller
{
    private readonly CustomerService _customerService;

    public CustomerController(
        CustomerService customerService
        )
    {
        this._customerService = customerService;
    }

    public async Task<IActionResult> Index()
    {
        var customers = await this._customerService.GetAllCustomers();

        // TODO: view model

        return this.View();
    }
}
