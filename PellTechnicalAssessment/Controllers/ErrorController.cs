﻿using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using PellTechnicalAssessment.Models;

namespace PellTechnicalAssessment.Controllers;

public class ErrorController : Controller
{
    [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
    public IActionResult Index()
    {
        return this.View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? this.HttpContext.TraceIdentifier });
    }
}
