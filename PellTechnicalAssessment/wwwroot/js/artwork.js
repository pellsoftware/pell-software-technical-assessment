﻿(() => {
    // https://api.artic.edu/docs/#iiif-image-api

    const apiEndPoint = 'https://api.artic.edu/api/v1/artworks/search?q=cats';

    const displayContainer = document.querySelector('div#art-data');
    const buttonElement = document.querySelector('button#art-search');

    buttonElement.addEventListener('click', () => {
        fetch(apiEndPoint)
            .then(e => e.json())
            .then(populateData)
            .catch(e => console.error(e));
    });

    function populateData(incoming) {
        const { data } = incoming;

        data.forEach(item => {
            const { thumbnail, title } = item;

            displayContainer.innerHTML += `
            <div class="col-auto">
            <div class="card" style="width: 18rem;">
              <img src="${thumbnail.lqip}" class="card-img-top" alt="${thumbnail.alt_text}">
              <div class="card-body">
                <h5 class="card-title">${title}</h5>
                <button class="btn btn-primary">Load Full</button>
              </div>
            </div>
            </div>
            `;
        });
    }

})();