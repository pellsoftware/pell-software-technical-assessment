﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PellTechnicalAssessment.Domain;

public class Customer
{
    [Key]
    public int CustomerId { get; set; }

    [StringLength(4)]
    [Column(TypeName = "varchar")]
    [Required]
    public string Name { get; set; } = null!;

    [StringLength(255)]
    [Column(TypeName = "varchar")]
    [Required]
    public string EmailAddress { get; set; } = null!;
}
